import React from 'react'

const header = () => {
    return (
        <header>
            <div>
                <h1>Todo app</h1>
                <p>Make sure you don't forget something :)</p>
            </div>
        </header>
    )
}

export default header