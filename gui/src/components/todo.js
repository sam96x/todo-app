import React from "react"

const todo = (props) => {
    return (
            //Položka todo
            <div className="todo-item">
                <h3 id="input">{props.text}</h3>
                <div className="todo-buttons">
                    <button className="update" onClick={()=>props.updateTodo(props.id)}>Update</button>
                    <button className="delete" onClick={()=>props.removeTodo(props.id)}>Delete</button>
                </div>       
            </div>     
        
    )
}

export default todo