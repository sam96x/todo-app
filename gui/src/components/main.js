import React from 'react'
import AddTodo from './addTodo'
import Todos from './todos'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid';


export default class Main extends React.Component {

    constructor(props) {
        super(props)
        this.addTodo = this.addTodo.bind(this)
        this.updateTodo = this.updateTodo.bind(this)
        this.removeTodos = this.removeTodos.bind(this)
        this.removeTodo = this.removeTodo.bind(this)
        this.state = {
          count: 0,
          todos: []
        }
    }

    //pridanie todo do listu
    addTodo = (todo) => {

        if (!todo) {
          return 'You have to write something'
        }

       axios({
          method: 'post',
          url: 'http://localhost:8081/todoapp/todos',
          data: {
            description: todo,
            uuid: uuidv4()
         }

       }).then(
          this.setState((prevState) => ({
            todos: prevState.todos.concat({"uuid": uuidv4(), "description": todo})
          }))
       )

    }

    //načíta dáta do komponentu
    componentDidMount() {
        axios({
          method: 'get',
          url: 'http://localhost:8081/todoapp/todos',
        }).then(response => 
          sessionStorage.setItem('todos', JSON.stringify(response.data))
          )
      
        const json = sessionStorage.getItem('todos')
        const todos = JSON.parse(json)
        if(todos) {
            this.setState(() => ({todos: todos}))
        }
    }

    //

    //aktualizuje komponent, ak sa zmení jeho stav
    componentDidUpdate(prevState) {
        if(prevState.todos !== this.state.todos) {
            const data = JSON.stringify(this.state.todos)
            sessionStorage.setItem('todos', data)
        }
    }


    //Vymazanie konkrétneho todo, vytvori sa nové pole bez už starého todo, ešte nie je funkčné
    removeTodo = (removeItem) => {       
        axios({
          method: 'delete',
          url: `http://localhost:8081/todoapp/todos/${removeItem}`,
        }).then(this.setState((prevState) => ({
              todos: prevState.todos.filter((option) => removeItem !== option)
            }))
        )
        
    }

    //vymazanie všetkých todos
    removeTodos = () => {
        axios({
          method: 'delete',
          url: 'http://localhost:8081/todoapp/todos',
        }).then(
          this.setState(() => ({
            todos: []
          }))
        )       
    }

    
    //Aktualizácia todo, ešte nie je funkčné
    updateTodo = (updateTodo) => {
      const elementh=document.getElementById('input')
      const elementinput = document.createElement('input')
      elementinput.setAttribute('type','text')
      elementinput.setAttribute('value', elementh.textContent)
      elementh.parentNode.replaceChild(elementinput, elementh)
    }

    render() {
        return(
            <main>
                <AddTodo addTodo={this.addTodo} getData={this.a}/>
                <Todos
                    todos={this.state.todos}
                    updateTodo={this.updateTodo}
                    removeTodos={this.removeTodos}
                    removeTodo={this.removeTodo}
                />
            </main>
        )
    }

}

