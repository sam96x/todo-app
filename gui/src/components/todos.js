import React from 'react'
import Todo from './todo'

const Todos = (props) => {
    return (
        //Implementácia todo listu
        <div>
            {props.todos.length === 0 && <p className="pleaseMessage">Please, add some todos</p>}
            {props.todos.length > 0 && 
            <div className="todo-header">
                <h3>Your todos</h3>
                {props.todos.length > 1 && <button className="delete" onClick={props.removeTodos}>Remove all</button>}
            </div>} 
            <div className="todos-list">
            {
                props.todos.map(
                    (todo) =>
                        <Todo 
                            key={todo.uuid}
                            id={todo.uuid}
                            text={todo.description}
                            removeTodo={props.removeTodo}
                            updateTodo={props.updateTodo}
                        />

                    )
            }
            </div>
        </div>
        
    )
}

export default Todos