import React from "react";

export default class AddTodo extends React.Component {
    
    constructor(props) {
        super(props)
        this.addTodo = this.addTodo.bind(this)
        this.state = {
            error: undefined
        }
    }
    
    addTodo = (e) => {
        e.preventDefault()
        const option = e.target.elements.todo.value.trim()
        const error = this.props.addTodo(option)
        
        this.setState({
            error: error
        })

        
        e.target.elements.todo.value = ''
    }
    
    render() {
        return(
            <form onSubmit={this.addTodo}>
                <input type="text" name="todo"/>
                <button>Add todo</button>
            </form>
        )
    }
}