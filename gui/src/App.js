import React from "react";
import Header from "./components/header.js"
import Footer from "./components/footer.js"
import Main from "./components/main.js"

export default class App extends React.Component {
    render() {
        return (
          <div>
              <Header />
              <Main className="container" />
              <Footer />
          </div>
        );
     }
}

