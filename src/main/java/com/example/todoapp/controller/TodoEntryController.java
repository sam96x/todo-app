package com.example.todoapp.controller;

import com.example.todoapp.model.TodoEntry;
import com.example.todoapp.repository.TodosRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("todoapp")
@CrossOrigin
public class TodoEntryController {

    private TodosRepository todosRepository;

    /**Konštruktor*/
    public TodoEntryController(TodosRepository todosRepository) {
        this.todosRepository = todosRepository;
    }

    /** Metóda na vypísanie všetkých položiek todos*/
    @GetMapping("/todos")
    public List<TodoEntry> getAllTodos() {
        return todosRepository.findAll();
    }

    /** Metóda na vypísanie konkrétnej todo na základe id
     * @return*/
    @GetMapping("/todos/{id}")
    public Optional<TodoEntry> getTodoById(@PathVariable Long id) {
        return todosRepository.findById(id);
    }

    /** Metóda, ktorá vloží todo do databázy
     * @return*/
    @PostMapping("/todos")
    public TodoEntry addTodo(@RequestBody TodoEntry todoEntry) {
        return todosRepository.save(todoEntry);
    }

    /** Metóda na vymazanie všetkých todos*/
    @DeleteMapping("/todos")
    public void removeAllTodos() {
        todosRepository.deleteAll();
    }

    /** Metóda na vymazanie konkrétneho todo */
    /*@DeleteMapping("/todos/{id}")
    public String removeTodoById(@RequestParam(value="uuid") String UUID) {

        return todosRepository.delete(UUID);
    }*/

}
