package com.example.todoapp.repository;

import com.example.todoapp.model.TodoEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TodosRepository extends JpaRepository<TodoEntry, Long> {


    @Transactional
    @Modifying
    @Query("DELETE FROM TodoEntry WHERE uuid = :uuid")
    String deleteByName(String uuid);
}
